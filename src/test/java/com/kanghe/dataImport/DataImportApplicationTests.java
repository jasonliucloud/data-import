package com.kanghe.dataImport;

import cn.hutool.json.JSONNull;
import cn.hutool.json.JSONUtil;
import com.kanghe.dataImport.config.HelloWorldConfig;
import com.kanghe.dataImport.entity.KbmClinicalSymptoms;
import com.kanghe.dataImport.mapper.KbmClinicalSymptomsMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DataImportApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ContextConfiguration(classes = HelloWorldConfig.class)
@Slf4j
class DataImportApplicationTests {
    @Autowired
    private KbmClinicalSymptomsMapper kbmClinicalSymptomsMapper;

    @Test
    void contextLoads() {

		KbmClinicalSymptoms result1 = kbmClinicalSymptomsMapper.findOne(kbmClinicalSymptomsMapper.query()
				.where.clinicalName().eq("水肿").end());
		log.info(JSONUtil.toJsonStr(result1));
    }

}
