package com.kanghe.dataImport.controller;

import com.kanghe.dataImport.service.ImportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/import")
@Api(tags = "ImportExcel")
public class ImportExcelController {
    @Autowired
    private ImportService importService;
    @ApiOperation("导入")
    @PostMapping("/import")
    public void importExcel(@ApiParam(value = "上传excel文件", required = true) @RequestPart("file") MultipartFile file) throws Exception {
        importService.importExcel(file);
    }
}
