package com.kanghe.dataImport.util;

import lombok.Getter;

/**
 * @Description
 * @Author HHJ
 * @Date 2019-08-08 14:44
 */
@Getter
public class BuzException extends RuntimeException {

    private Integer code = 1001;

    public BuzException(String msg1) {
        super(msg1);
    }

    public BuzException(Integer code1) {
        super();
        this.code = code1;
    }

    public BuzException(Integer code1, String msg1) {
        super(msg1);
        this.code = code1;
    }
}
