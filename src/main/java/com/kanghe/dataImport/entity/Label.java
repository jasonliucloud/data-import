package com.kanghe.dataImport.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

@Data
public class Label implements java.io.Serializable {
    @Excel(name = "症状名称", width = 30, orderNum = "1", isImportField = "true",needMerge = true)
    private String name1;
    @Excel(name = "对应部位", width = 30, orderNum = "1", isImportField = "true",needMerge = true)
    private String name2;
    @Excel(name = "轻度", width = 30, orderNum = "1", isImportField = "true",needMerge = true)
    private String name3;
    @Excel(name = "中度", width = 30, orderNum = "1", isImportField = "true",needMerge = true)
    private String name4;
    @Excel(name = "重度", width = 30, orderNum = "1", isImportField = "true",needMerge = true)
    private String name5;
}
