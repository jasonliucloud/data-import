package com.kanghe.dataImport.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * <p>
 * 临床症状表
 * </p>
 *
 * @author zhangkp
 */
@Data
@Accessors(chain = true)
@FluentMybatis(table="t_kbm_clinical_symptoms")
public class KbmClinicalSymptoms extends RichEntity {

    private Long id;

    /**
     * 临床code
     */
    private String clinicalCode;

    /**
     * 临床症状名称
     */
    private String clinicalName;

    /**
     * 备注
     */
    private String remark;

    private Integer delFlag;

    private String creator;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String updator;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @Override
    public Class<? extends IEntity> entityClass() {
        return KbmClinicalSymptoms.class;
    }
}