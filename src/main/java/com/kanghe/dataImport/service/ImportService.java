package com.kanghe.dataImport.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.pinyin.PinyinUtil;
import cn.hutool.json.JSONUtil;
import com.kanghe.dataImport.entity.KbmClinicalSymptoms;
import com.kanghe.dataImport.entity.Label;
import com.kanghe.dataImport.mapper.KbmClinicalSymptomsMapper;
import com.kanghe.dataImport.util.ExcelUtils;
import com.vip.vjtools.vjkit.time.DateFormatUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@Service
@Slf4j
public class ImportService {
    @Autowired
    private KbmClinicalSymptomsMapper kbmClinicalSymptomsMapper;

    public void importExcel(MultipartFile file) throws Exception {
        List<Label> labelList = ExcelUtils.importExcel(file, 1, 1, Label.class);
        System.out.println("成功导入..............." + labelList.size());
        int startId=400;
        Map<String, String> bfLable = new TreeMap<>();
        for (Label label : labelList) {
            if (StrUtil.isNotBlank(label.getName2())) {
                if (label.getName2().contains("、")) {
                    String labels[] = label.getName2().split("、");
                    for (String label1 : labels) {
                        bfLable.put(label1, label1);
                    }
                } else {
                    bfLable.put(label.getName2(), label.getName2());
                }
            }
//            System.out.println(label.getName1());
            KbmClinicalSymptoms kbmClinicalSymptoms = kbmClinicalSymptomsMapper.findOne(kbmClinicalSymptomsMapper.query()
                    .where.clinicalName().eq(label.getName1()).end());
            String parts = "";
            String partsName = "";
            if(StrUtil.isNotBlank(label.getName2())) {
                partsName = label.getName2().replaceAll("、", ",");
                if (partsName.contains(",")) {
                    for (int i = 0; i < partsName.split(",").length; i++) {
                        parts += "symptom_part_" + PinyinUtil.getPinyin(partsName.split(",")[i]).replaceAll(" ", "");
                        if ((i + 1) < partsName.split(",").length) {
                            parts += ",";
                        }
                    }
                } else {
                    parts = "symptom_part_" + PinyinUtil.getPinyin(partsName).replaceAll(" ", "");
                }
            }
            if (kbmClinicalSymptoms != null) {
                if(StrUtil.isNotBlank(label.getName2())){
                    String updateSql = "UPDATE `db_ncdp_health`.`t_kbm_clinical_symptoms` SET `parts` = '" + parts + "', `parts_name` = '" + partsName + "' WHERE `id` = " + kbmClinicalSymptoms.getId();
                    System.out.println(updateSql);
                }
                if(StrUtil.isNotBlank(label.getName3())){
                    String insertDegreeSql="INSERT INTO `db_ncdp_health`.`t_kbm_clinical_sympotoms_degree`(`clinical_id`, `degree_code`, `degree_name`, `degree_detail`) VALUES " +
                            "("+ kbmClinicalSymptoms.getId() +", 'symptom_degree_mild', '轻度', '"+label.getName3()+"');";
                    System.out.println(insertDegreeSql);
                }
                if(StrUtil.isNotBlank(label.getName4())){
                    String insertDegreeSql="INSERT INTO `db_ncdp_health`.`t_kbm_clinical_sympotoms_degree`(`clinical_id`, `degree_code`, `degree_name`, `degree_detail`) VALUES " +
                            "("+ kbmClinicalSymptoms.getId() +", 'symptom_degree_mild', '轻度', '"+label.getName4()+"');";
                    System.out.println(insertDegreeSql);
                }
                if(StrUtil.isNotBlank(label.getName5())){
                    String insertDegreeSql="INSERT INTO `db_ncdp_health`.`t_kbm_clinical_sympotoms_degree`(`clinical_id`, `degree_code`, `degree_name`, `degree_detail`) VALUES " +
                            "("+ kbmClinicalSymptoms.getId() +", 'symptom_degree_mild', '轻度', '"+label.getName5()+"');";
                    System.out.println(insertDegreeSql);
                }
            } else {
                String ccode="ZZGL"+String.format("%04d",startId);
                String cdate = DateFormatUtil.formatDate("yyyy-MM-dd HH:mm:ss",com.vip.vjtools.vjkit.time.DateUtil.addSeconds(new Date(),startId));
                String innsertSql = "INSERT INTO `db_ncdp_health`.`t_kbm_clinical_symptoms`(`id`, `clinical_code`, `clinical_name`, `remark`, `del_flag`, `creator`, `create_time`, `updator`, `update_time`, `propaganda_code`, `parts`, `parts_name`) VALUES " +
                        "("+startId+", '"+ccode+"', '"+label.getName1()+"', NULL, 0, 'admin', '"+cdate+"', '系统管理员', '"+cdate+"', NULL,"+(StrUtil.isNotBlank(parts)?"'"+parts+"',":"NULL,")+(StrUtil.isNotBlank(partsName)?"'"+partsName+"');,":"NULL);");
                System.out.println(innsertSql);

                if(StrUtil.isNotBlank(label.getName3())){
                    String insertDegreeSql="INSERT INTO `db_ncdp_health`.`t_kbm_clinical_sympotoms_degree`(`clinical_id`, `degree_code`, `degree_name`, `degree_detail`) VALUES " +
                            "("+ startId +", 'symptom_degree_mild', '轻度', '"+label.getName3()+"');";
                    System.out.println(insertDegreeSql);
                }
                if(StrUtil.isNotBlank(label.getName4())){
                    String insertDegreeSql="INSERT INTO `db_ncdp_health`.`t_kbm_clinical_sympotoms_degree`(`clinical_id`, `degree_code`, `degree_name`, `degree_detail`) VALUES " +
                            "("+ startId +", 'symptom_degree_mild', '轻度', '"+label.getName4()+"');";
                    System.out.println(insertDegreeSql);
                }
                if(StrUtil.isNotBlank(label.getName5())){
                    String insertDegreeSql="INSERT INTO `db_ncdp_health`.`t_kbm_clinical_sympotoms_degree`(`clinical_id`, `degree_code`, `degree_name`, `degree_detail`) VALUES " +
                            "("+ startId +", 'symptom_degree_mild', '轻度', '"+label.getName5()+"');";
                    System.out.println(insertDegreeSql);
                }
                startId++;
            }
        }
//        String mainId=UUID.randomUUID().toString().replaceAll("-", "");
//        System.out.println("INSERT INTO `db_ncdp_classify`.`label`(`id`, `label_name`, `parent_id`, `label_code`, `describes`, `create_date`, `update_date`, `del_flag`, `remarks`, `label_level`) VALUES ('"+mainId+"', '部位', '0', 'symptom_part', NULL, '"+ DateUtil.now() +"', '"+ DateUtil.now() +"', '0', NULL, NULL);");
//        int count1=1;
//        for (Map.Entry<String, String> entry : bfLable.entrySet()) {
//           String cdate = DateFormatUtil.formatDate("yyyy-MM-dd HH:mm:ss",com.vip.vjtools.vjkit.time.DateUtil.addSeconds(new Date(),count1));
//           String insertLabelSql="INSERT INTO `db_ncdp_classify`.`label`(`id`, `label_name`, `parent_id`, `label_code`, `describes`, `create_date`, `update_date`, `del_flag`, `remarks`, `label_level`) VALUES ("
//                    +"'"+UUID.randomUUID().toString().replaceAll("-", "")+"','"+entry.getValue()+"','"+mainId+"','symptom_part_"+ PinyinUtil.getPinyin(entry.getValue()).replaceAll(" ", "")+"', NULL, '"+ cdate +"', '"+ cdate +"', '0', NULL, NULL);";
//            System.out.println(insertLabelSql);
//            count1++;
//        }
    }

}